using System;
using MyConfig;
using NUnit.Framework;

namespace UnitTests
{
	internal class TestSettings {
		public int Count { get;set; }
	};

	internal class InvalidTestSetting {
		public TestSettings TestSettings { get; set; }
	}

	internal class TestConfigurationProvider : IConfigurationProvider {
		bool IConfigurationProvider.TryGetSetting(string name, out IConvertible value) {
			value = name;
			return true;
		}
	}

	[TestFixture]
	public class ConfigurationManagerTests {

		[Test]
		public void Constructor_WhenInvalidAssembly_ThrowException() {
			var exception = Assert.Throws<ConfigurationException>(() => new ConfigurationManager<TestSettings>("System.ProviderDoesNotExist", string.Empty));
			Assert.AreEqual(ConfigurationErrorCode.ASSEMBLY_NAME_NOT_FOUND, exception.ErrorCode);
		}

		[Test]
		public void Constructor_WhenAssemblyDoesNotSupportInterface_ThrowException() {
			var exception = Assert.Throws<ConfigurationException>(() => new ConfigurationManager<TestSettings>(typeof(Object).AssemblyQualifiedName, string.Empty));
			Assert.AreEqual(ConfigurationErrorCode.INTERFACE_NOT_IMPLEMENTED, exception.ErrorCode);
		}

		[Test]
		public void Constructor_WhenValidAssembly_DoNotThrowException() {
			var manager = new ConfigurationManager<TestSettings>(typeof(TestConfigurationProvider).AssemblyQualifiedName, string.Empty);
			Assert.IsNotNull(manager);
		}

		[Test]
		public void GetSetting_WhenPropertyIsNotIConvertible_ThrowException() {
			var manager = new ConfigurationManager<InvalidTestSetting>(typeof(TestConfigurationProvider).AssemblyQualifiedName, string.Empty);
			Assert.IsNotNull(manager);
			var exception = Assert.Throws<ConfigurationException>(() => manager.GetSettings());
			Assert.AreEqual(ConfigurationErrorCode.PROPERTY_NOT_ICONVERTIBLE, exception.ErrorCode);
		}

		[Test]
		public void GetSetting_WhenPropertyConversionFails_ThrowException() {
			var manager = new ConfigurationManager<TestSettings>(typeof(TestConfigurationProvider).AssemblyQualifiedName, string.Empty);
			Assert.IsNotNull(manager);
			var exception = Assert.Throws<ConfigurationException>(() => manager.GetSettings());
			Assert.AreEqual(ConfigurationErrorCode.INVALID_CONVERSION, exception.ErrorCode);

		}
	}
}

