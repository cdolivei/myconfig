# MyConfig

Sane-ish Configuration Management

MyConfig is a replacement for [.NET's ConfigurationManager] [1]. Rather than rely on a AppSettings properties, MyConfig will populate a Plain Old C# Object (POCO) from a source. This source can be App.config, a XML file, a database, or you can create your own plugin.

Right now the only plugin supported is App.config

## Benefits/Drawbacks

Benefits:

* Settings are strongly typed rather than relying on a string
* Settings that are declared but not defined (because a teammate added a new setting but didn't tell you) will raise an exception when settings are loaded
* You can store your settings outside AppSettings.config

Drawbacks:

* Only support App.config
* Can't handle data types like arrays
* No way to specify default values

## Quickstart

* Add a reference to MyConfig and the plugin you are using as the source
* Create a POCO object that contains only getter/setter properties. You can use strings, datetime, ints, or anything that supports [IConvertible] [2] :
```
#!c#
class MySettings {
	public string MaxBackgroundThreads { get; set; }
}
```
* Grab your settings!
```
#!c#
var manager = new ConfigurationManager<ConfigurationSettings>(MyConfig.Plugins.AppSettingsConfigProvider.ASSEMBLY_NAME);
var settings = manager.GetSettings();
```

 [1]: http://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager%28v=vs.110%29.aspx
 [2]: http://msdn.microsoft.com/en-us/library/system.iconvertible%28v=vs.110%29.aspx

