using System;
using System.Configuration;

namespace MyConfig.Plugins {
	public class AppSettingsConfigProvider : IConfigurationProvider {
		public static readonly string ASSEMBLY_NAME = typeof(AppSettingsConfigProvider).AssemblyQualifiedName;

		public bool TryGetSetting(string name, out IConvertible value) {
			value = ConfigurationManager.AppSettings[name];
			return value != null;
		}
	}
}
