using System;

namespace MyConfig {
	public interface IConfigurationProvider {
		bool TryGetSetting(string name, out IConvertible value);
	}
}
