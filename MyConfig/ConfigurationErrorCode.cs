namespace MyConfig
{
	public enum ConfigurationErrorCode {
		ASSEMBLY_NAME_NOT_FOUND,	 		// Assembly name in configuration not found
		INTERFACE_NOT_IMPLEMENTED,			// Assembly name does not implement IMyConfigConfigurationProvider
		PROPERTY_NOT_ICONVERTIBLE,			// Configuration property does not implement IConvertible
		SETTING_NOT_FOUND,					// No value found for configuration property
		INVALID_CONVERSION,					// Unable to convert setting to Confgiuration property type
	}
}

