using System;
using System.Configuration;
using System.Globalization;

namespace MyConfig {
	public sealed class ConfigurationManager<T> where T:class {
		private IConfigurationProvider m_provider;

		public ConfigurationManager(IConfigurationProvider provider) {
			m_provider = provider;
		}

		public ConfigurationManager(string assembly, string settings = null) {
			if (string.IsNullOrEmpty(assembly)) {
				throw new ArgumentException("Parameter cannot be null or empty", "assembly");
			}

			Initialize(assembly, settings);
		}

		private void Initialize (string assembly, string settings = null) {
			var type = Type.GetType(assembly);

			if (type == null) {
				throw new ConfigurationException(ConfigurationErrorCode.ASSEMBLY_NAME_NOT_FOUND);
			}

			if (string.IsNullOrEmpty(settings)) {
				m_provider = Activator.CreateInstance(type) as IConfigurationProvider;
			}
			else {
				m_provider = Activator.CreateInstance(type, settings) as IConfigurationProvider;
			}

			if (m_provider == null) {
				throw new ConfigurationException(ConfigurationErrorCode.INTERFACE_NOT_IMPLEMENTED, assembly);
			}
		}

		public T GetSettings() {
			var type = typeof(T);
			var instance = Activator.CreateInstance(type) as T;

			foreach (var propertyInfo in type.GetProperties()) {
				var propertyType = propertyInfo.PropertyType;
				var propertyName = propertyInfo.Name;

				if (propertyType.GetInterface(typeof(IConvertible).Name) == null) {
					throw new ConfigurationException(ConfigurationErrorCode.PROPERTY_NOT_ICONVERTIBLE, propertyName, type.Name);
				}

				IConvertible settingValue;
				if (!m_provider.TryGetSetting(propertyName, out settingValue) || settingValue == null) {
					throw new ConfigurationException(ConfigurationErrorCode.SETTING_NOT_FOUND, propertyName);
				}

				object value;
				try {
					value = settingValue.ToType(propertyType, CultureInfo.CurrentCulture);
				} catch (FormatException ex) {
					throw new ConfigurationException(ConfigurationErrorCode.INVALID_CONVERSION, ex, propertyName, propertyType.Name);
				}
				propertyInfo.SetValue(instance, value, null);
			}

			return instance;
		}
	}
}
