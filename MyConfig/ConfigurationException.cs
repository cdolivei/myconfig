using System;

namespace MyConfig {
	public class ConfigurationException : Exception {
		public ConfigurationErrorCode ErrorCode {
			get;
			private set;
		}

		public ConfigurationException (ConfigurationErrorCode errorCode, Exception innerException, params object[] args) :
			base(GetMessageFromCode(errorCode, args), innerException) {
			ErrorCode = errorCode;
		}

		public ConfigurationException (ConfigurationErrorCode errorCode, params object[] args) :
			base(GetMessageFromCode(errorCode, args)) {
			ErrorCode = errorCode;
		}

		private static string GetMessageFromCode(ConfigurationErrorCode errorCode, params object[] args) {
			var message = string.Empty;
			switch (errorCode) {
				case ConfigurationErrorCode.ASSEMBLY_NAME_NOT_FOUND:
					message = "Could not find the assembly name. The assembly name is null or invalid.";
					break;
				case ConfigurationErrorCode.INTERFACE_NOT_IMPLEMENTED:
					message = "{0} is not a valid IMyConfigConfigurationProvider";
					break;
				case ConfigurationErrorCode.PROPERTY_NOT_ICONVERTIBLE:
					message = "Property {0} of {1} does not implement IConvertible";
					break;
				case ConfigurationErrorCode.SETTING_NOT_FOUND:
					message = "Could not find setting for {0}";
					break;
				case ConfigurationErrorCode.INVALID_CONVERSION:
					message = "Could not convert setting {0} into type {1}";
					break;
			}
			return string.Format(message, args);
		}
	}
}
